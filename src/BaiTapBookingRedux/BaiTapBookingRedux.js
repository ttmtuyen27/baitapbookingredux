import React, { Component } from "react";
import Infor from "./Infor";
import SeatList from "./SeatList";

export default class BaiTapBookingRedux extends Component {
  render() {
    return (
      <>
        <div id="bgImg">
          <div id="content" className="row container py-5 mx-5">
            <div className="col-8">
              <SeatList />
            </div>
            <div className="col-4 pl-5">
              <Infor />
            </div>
          </div>
        </div>
      </>
    );
  }
}
