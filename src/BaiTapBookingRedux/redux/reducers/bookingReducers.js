import { dataSeat } from "../../dataSeat";
import {
  CHOOSE_SEAT,
  CONFIRM_SEAT,
  INITIAL_RENDER,
} from "../constants/constants";

let initialState = {
  dataSeat: dataSeat,
  infor: [],
};

export const bookingReducers = (state = initialState, { type, payload }) => {
  switch (type) {
    case INITIAL_RENDER: {
      let cloneSeatList = [...state.dataSeat];
      cloneSeatList.forEach((item) => {
        item.danhSachGhe.forEach((seat) => {
          if (seat.daDat == true) seat["daXacNhan"] = true;
          else seat["daXacNhan"] = false;
        });
      });
      return { dataSeat: cloneSeatList };
    }
    case CHOOSE_SEAT: {
      let cloneSeatList = [...state.dataSeat];
      let indexHang = cloneSeatList.findIndex((item) => {
        return item.hang == payload.soHang;
      });
      let indexGhe = cloneSeatList[indexHang].danhSachGhe.findIndex((seat) => {
        return seat.soGhe == payload.soGhe;
      });
      if (cloneSeatList[indexHang].danhSachGhe[indexGhe].daDat == true) {
        cloneSeatList[indexHang].danhSachGhe[indexGhe].daDat = false;
      } else cloneSeatList[indexHang].danhSachGhe[indexGhe].daDat = true;
      return { dataSeat: cloneSeatList };
    }
    case CONFIRM_SEAT: {
      let cloneSeatList = [...state.dataSeat];
      let cloneInfor = [];

      cloneSeatList.forEach((item) => {
        item.danhSachGhe.forEach((seat) => {
          if (seat.daDat === true && seat.daXacNhan === false) {
            seat.daXacNhan = true;
            cloneInfor.push(seat);
          }
        });
      });
      return { dataSeat: cloneSeatList, infor: cloneInfor };
    }
    default:
      return state;
  }
};
