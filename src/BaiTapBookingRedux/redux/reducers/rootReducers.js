import { bookingReducers } from "./bookingReducers";
import { combineReducers } from "redux";

export const rootReducers = combineReducers({ bookingReducers });
