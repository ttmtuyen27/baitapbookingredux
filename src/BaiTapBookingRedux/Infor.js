import React, { Component, PureComponent } from "react";
import { connect } from "react-redux";

class Infor extends PureComponent {
  render() {
    let totalPurchase = 0;
    return (
      <div
        className="my-5"
        style={{
          width: "350px",
          height: "max-content",
        }}
      >
        <div className="row mx-2 mt-4">
          <div className="ghe"></div>
          <p className="mx-3 text-white" style={{ fontSize: "18px" }}>
            Available
          </p>
        </div>
        <div className="row mx-2">
          <div className="gheDuocChon"></div>
          <p className="mx-3 text-white" style={{ fontSize: "18px" }}>
            Booked
          </p>
        </div>
        <div className="row mx-2">
          <div className="gheDangChon "></div>
          <p className="mx-3 text-white " style={{ fontSize: "18px" }}>
            Selected
          </p>
        </div>
        {this.props.infor && (
          <div className="mt-3">
            <p style={{ fontSize: "20px", fontWeight: "bold", color: "white" }}>
              Booking information
            </p>
            <table className="table table-dark table-bordered">
              <thead style={{ backgroundColor: "orange", color: "black" }}>
                <tr>
                  <th>Seat Number</th>
                  <th>Price</th>
                </tr>
              </thead>
              <tbody className="bg-light text-dark">
                {this.props.infor?.map((item) => {
                  totalPurchase += item.gia;
                  return (
                    <tr style={{ fontWeight: "bold", fontSize: "15px" }}>
                      <td>{item.soGhe}</td>
                      <td>{item.gia}</td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            <div
              style={{ fontWeight: "bold", fontSize: "20px", color: "white" }}
            >
              TOTAL: <span style={{ color: "red" }}>{totalPurchase} </span>
            </div>
          </div>
        )}
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    infor: state.bookingReducers.infor,
  };
};

export default connect(mapStateToProps)(Infor);
