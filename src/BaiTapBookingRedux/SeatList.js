import React, { Component } from "react";
import { connect } from "react-redux";
import {
  CHOOSE_SEAT,
  CONFIRM_SEAT,
  INITIAL_RENDER,
} from "./redux/constants/constants";

class SeatList extends Component {
  UNSAFE_componentWillMount() {
    this.props.initializeRender();
  }
  render() {
    return (
      <>
        <h4 className="text-left text-white">
          Please select the seat(s) you want to book:
        </h4>
        {this.props.seatList.map((item) => {
          return (
            <div className="row">
              <div
                style={{
                  width: "10px",
                  color: "red",
                  fontWeight: "bold",
                }}
              >
                {item.hang}
              </div>
              {item.danhSachGhe.map((seat) => {
                if (seat.daDat == false) {
                  return (
                    <div
                      className="ghe"
                      onClick={() => {
                        this.props.handleChooseSeat(item.hang, seat.soGhe);
                      }}
                      style={{ fontWeight: "700" }}
                    >
                      {seat.soGhe}
                    </div>
                  );
                } else if (seat.daDat == true) {
                  if (seat.daXacNhan == true) {
                    return (
                      <div
                        className="gheDuocChon"
                        style={{ fontWeight: "700" }}
                      >
                        {seat.soGhe}
                      </div>
                    );
                  } else
                    return (
                      <div
                        className="gheDangChon"
                        onClick={() => {
                          this.props.handleChooseSeat(item.hang, seat.soGhe);
                        }}
                        style={{ fontWeight: "700" }}
                      >
                        {seat.soGhe}
                      </div>
                    );
                } else {
                  return (
                    <div
                      className="ghe border-0"
                      style={{
                        fontSize: "20px",
                        fontWeight: "bold",
                        color: "red",
                        backgroundColor: "transparent",
                      }}
                    >
                      {seat.soGhe}
                    </div>
                  );
                }
              })}
            </div>
          );
        })}
        <div
          style={{
            width: "83%",
            height: "50px",
            backgroundColor: "orange",
            margin: "10px 5px",
            color: "white",
            fontSize: "20px",
            paddingTop: "7px",
            fontWeight: "bold",
            borderRadius: "5px",
          }}
        >
          SCREEN
        </div>

        <button
          onClick={this.props.handleConfirmSeat}
          className="btn btn-success px-3"
          style={{
            backgroundColor: "#95CD41",
            fontWeight: "bold",
            border: "none",
            marginRight: "110px",
            marginTop: "10px",
          }}
        >
          Confirm
        </button>
      </>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    seatList: state.bookingReducers.dataSeat,
    infor: state.bookingReducers.infor,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    initializeRender: () => {
      dispatch({
        type: INITIAL_RENDER,
      });
    },
    handleChooseSeat: (soHang, soGhe) => {
      dispatch({
        type: CHOOSE_SEAT,
        payload: { soHang: soHang, soGhe: soGhe },
      });
    },
    handleConfirmSeat: () => {
      dispatch({
        type: CONFIRM_SEAT,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SeatList);
