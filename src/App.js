import logo from "./logo.svg";
import "./App.css";
import BaiTapBookingRedux from "./BaiTapBookingRedux/BaiTapBookingRedux";

function App() {
  return (
    <div className="App">
      <BaiTapBookingRedux />
    </div>
  );
}

export default App;
